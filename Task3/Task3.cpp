#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
/*
�������� �� ��������� ������:
1. �������� �������, �� ������ ���� �������� � ������� ����� �
��������� �������� ����. ���������, ��� 7631 ������� ������� ��������� 1367.
*/
int reverse(int i) {
	int r = 0;
	while (i > 0) {
		r *= 10;
		r += i % 10;
		i /= 10;
	}
	return r;
}
int main() {
	int n;
	printf("Enter value:");
	scanf("%d", &n);
	printf("reversed value %d\n", reverse(n));
	return 0;
}
