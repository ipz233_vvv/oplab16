#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
/*
�������� 2. ���� �����. �������������� �������, ������� �������� ��������:
a) �������� ������� ���������� ���� �������� ������;
b) �������� ������� ����������� ������������� �������� �������� ������;
c) �������� ������� ����������� ����������� �������� �������� ������;
d) �������� ������� ���������� ������� �������� ������.

����� ������� ������������ � ������� �� ��������
*/
int sum(int m[], int size) {
	int s = 0;
	for (int i = 0; i < size; ++i) {
		s += m[i];
	}
	return s;
}

int amax(int m[], int size) {
	int mv = m[0];
	for (int i = 1; i < size; ++i) {
		mv = mv > m[i] ? mv : m[i];
	}
	return mv;
}

int amin(int m[], int size) {
	int mv = m[0];
	for (int i = 1; i < size; ++i) {
		mv = mv < m[i] ? mv : m[i];
	}
	return mv;
}

int product(int m[], int size) {
	int s = 1;
	for (int i = 0; i < size; ++i) {
		s *= m[i];
	}
	return s;
}

int main() {
	int m[100000];
	int n;
	printf("Enter array size and its elements:");
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &m[i]);
	}
	printf("Sum: %d\n", sum(m, n));
	printf("Max: %d\n", amax(m, n));
	printf("Min: %d\n", amin(m, n));
	printf("Product: %d\n", product(m, n));

	return 0;
}
