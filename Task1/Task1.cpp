#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
�������� 1. �������� ������� ���������� �����
�������������

S = a � b � sin alpha
*/
float area(float a, float b, float degree) {
	return a * b * sin(degree / 180 * M_PI);
}
int main() {
	float a, b, al;
	printf("Enter a, b, alpha (degrees):\n");
	scanf("%f%f%f", &a, &b, &al);
	printf("Area of parallelogram is %.2f sqr. units", area(a, b, al));
	return 0;
}